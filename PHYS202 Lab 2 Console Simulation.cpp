// PHYS202 Lab 2 Console Simulation.cpp by Timothy Evans: This file contains the 'main' function. Program execution begins and ends there.
#include <iostream>
#include <iomanip>
#include <string>
#include <windows.h>
#include <ctype.h>
#include <algorithm>
using namespace std;

HANDLE hConsole;

//declartion of used variables starting at time = 0s
const double torpedoMass = 0.5, electrostaticConstant = 8987551787, torpedoCharge = 16.0, deltat = 0.01;
double torpedoAccelerationX = NULL, torpedoAccelerationY = NULL, torpedoPositionX = 10000.00, torpedoPositionY = 500.00, 
torpedoVelocityX = -1200.00, torpedoVelocityY = 0.00, netForceOnTorpedoX = NULL, netForceOnTorpedoY = NULL;

//Used to find the distance between an individual shield and the torpedo
//sets distance = SQUAREROOT(TorpedoPosition(x)^2 + (ShieldPosition(y) - TorpedoPosition(y))^2)
void calculateDistance(double& distance, double& shieldPositionY, double& torpedoPositionX, double& torpedoPositionY)
{
    distance =  sqrt(pow(torpedoPositionX, 2.0) + pow((shieldPositionY - torpedoPositionY), 2.0));
}

//Used to find the electric field of an individual shield
//sets Electric Field(shield) = k|q(1)|/(r^2)
//k is electrostatic constant = 8.99x10E9 N m^2/C^2, q is electric charge of shield, r is distance from torpedo
void calculateElectricField(double& electricField, double& electricCharge, double& distanceFromTorpedo)
{
    electricField =  ((electrostaticConstant * electricCharge) / (pow(distanceFromTorpedo, 2.0)));
}

//Used to find the force of an individual shield on the torpedo
//sets Force(shield on torpedo) = |q(2)|E
//q is torpedo charge, E is deflector shield electric field
//Splits into forces in x and y direction using
//ForceX = Force * cos(theta), ForceY = -(Force * sin(theta)), theta = tan-1((Yshield - Ytorpedo) / Xtorpedo)
void calculateShieldForceOnTorpedo(double& shieldForceOnTorpedo, double& shieldForceOnTorpedoX, double& shieldForceOnTorpedoY, 
    double& torpedoPositionX, double& torpedoPositionY, double & shieldHeight, double& electricField)
{
    shieldForceOnTorpedo = (torpedoCharge * electricField);
    double theta = atan((shieldHeight - torpedoPositionY) / torpedoPositionX);
    shieldForceOnTorpedoX = (shieldForceOnTorpedo * cos(theta));
    shieldForceOnTorpedoY = -(shieldForceOnTorpedo * sin(theta));
}

//class for individual deflector shield containing its variables
//there will be 10 of these in the DeflectorShieldList class
class DeflectorShield
{
public:
    int shieldNumber;
    double electricCharge, forceOnTorpedo, forceOnTorpedoX, forceOnTorpedoY, height, distanceFromTorpedo, electricField;
    DeflectorShield* next; //points to deflector shield above current one in the list

    //default constructor for deflector shield, assigns null values
    DeflectorShield()
    {
        shieldNumber = NULL;
        electricCharge = NULL;
        forceOnTorpedo = NULL;
        forceOnTorpedoX = NULL;
        forceOnTorpedoY = NULL;
        height = NULL;
        distanceFromTorpedo = NULL;
        electricField = NULL;
        next = NULL;
    }

    //Overload '=' to set deflector shield equal to another existing one
    DeflectorShield& operator=(const DeflectorShield& shield)
    {
        shieldNumber = shield.shieldNumber;
        electricCharge = shield.electricCharge;
        forceOnTorpedo = shield.forceOnTorpedo;
        height = shield.height;
        distanceFromTorpedo = shield.distanceFromTorpedo;
        electricField = shield.electricField;
        *next = *shield.next;
    }
};

//class for circular linked list of 10 Deflector Shields starting from the bottom going up
class DeflectorShieldList
{
public:
    DeflectorShield* head; //pointer object, always points to first shield
    DeflectorShield* current; //points to current viewed shield in list

    //Default Constructor, assigns NULL pointers
    DeflectorShieldList()
    {
        current = head = NULL; 
    }


    //deletes all of the shield objects in the list
    void deleteShieldsInList()
    {
        if (current != head)
        {
            current = head;
        }

        DeflectorShield* nextToDelete = current->next;

        for (int i = 0; i < 10; i++)
        {
            current->shieldNumber = NULL;
            current->electricCharge = NULL;
            current->forceOnTorpedo = NULL;
            current->forceOnTorpedoX = NULL;
            current->forceOnTorpedoY = NULL;
            current->height = NULL;
            current->distanceFromTorpedo = NULL;
            current->electricField = NULL;
            current->next = NULL;
            current = nextToDelete;
        }

        delete nextToDelete;
        current = head = NULL;
    }
    
    //resets all shield variables back to t=0s after torpedo simulation runs
    void resetShieldVariables()
    {
        if (current != head)
        {
            current = head;
        }

        double charges[10];
        DeflectorShield* nextShield = current->next;

        for (int shield = 0; shield < 10; shield++)
        {
            charges[shield] = current->electricCharge;
            current = nextShield;
            if (shield != 9)
            {
                nextShield = current->next;
            }
        }
        nextShield = NULL;

        deleteShieldsInList();
        head = new DeflectorShield;
        current = head;

        double currentForce = NULL, currentForceX = NULL, currentForceY = NULL,
            currentDistance = NULL, currentElectricField = NULL, currentHeight = 50.0;

        for (int shield = 0; shield < 10; shield++)
        {
            calculateDistance(currentDistance, currentHeight, torpedoPositionX, torpedoPositionY);
            calculateElectricField(currentElectricField, charges[shield], currentDistance);
            calculateShieldForceOnTorpedo(currentForce, currentForceX, currentForceY, torpedoPositionX, torpedoPositionY, currentHeight, currentElectricField);

            current->shieldNumber = (shield + 1);
            current->height = currentHeight;
            current->electricCharge = charges[shield];
            current->forceOnTorpedo = currentForce;
            current->forceOnTorpedoX = currentForceX;
            current->forceOnTorpedoY = currentForceY;
            current->distanceFromTorpedo = currentDistance;
            current->electricField = currentElectricField;

            if (shield != 9)
            {
                current->next = new DeflectorShield;
                current = current->next;
                currentHeight += 100.0;
            }
        }
        current->next = head;
        currentHeight = NULL;
        fill_n(charges, 10, 0);
    }

    //Calculates the total net force of the deflector shields on the torpedo
    //Fnet(x) = (i=1)SUM(to 10) ForceShieldOnTorpedoX(i)
    void calculateNetForceOnTorpedoX(double& netForceOnTorpedoX)
    {
        if (current != head)
        {
            current = head;
        }

        netForceOnTorpedoX = 0.0;

        for (int shield = 0; shield < 10; shield++)
        {
            netForceOnTorpedoX += current->forceOnTorpedoX;
            current = current->next;
        }
    }

    //Calculates the total net force of the deflector shields on the torpedo
    //Fnet(y) = (i=1)SUM(to 10) ForceShieldOnTorpedoY(i)
    void calculateNetForceOnTorpedoY(double& netForceOnTorpedoY)
    {
        if (current != head)
        {
            current = head;
        }

        netForceOnTorpedoY = 0.0;

        for (int shield = 0; shield < 10; shield++)
        {
            netForceOnTorpedoY += current->forceOnTorpedoY;
            current = current->next;
        }
    }

    //Calculates Torpedo's Acceleration in the x direction
    //TorpedoAcceleration(x) = ForceShieldOnTorpedo(x) / MassTorpedo
    void calculateTorpedoAccelerationX(double& torpedoAccelerationX, double& netForceOnTorpedoX)
    {
        torpedoAccelerationX = (netForceOnTorpedoX / torpedoMass);
    }

    //Calculates Torpedo's Acceleration in the y direction
    //TorpedoAcceleration(y) = ForceShieldOnTorpedo(y) / MassTorpedo
    void calculateTorpedoAccelerationY(double& torpedoAccelerationY, double& netForceOnTorpedoY)
    {
        torpedoAccelerationY = (netForceOnTorpedoY / torpedoMass);
    }

    //Calculates Torpedo's Velocity in the x direction
    //TorpedoVelocityX(n) = TorpedoVelocityX(n-1) + (TorpedoAccelerationX * deltat)
    void calculateTorpedoVelocityX(double& torpedoVelocityX, double& torpedoAccelerationX)
    {
        torpedoVelocityX += (torpedoAccelerationX * deltat);
    }

    //Calculates Torpedo's Velocity in the y direction
    //TorpedoVelocityY(n) = TorpedoVelocityY(n-1) + (TorpedoAccelerationY * deltat)
    void calculateTorpedoVelocityY(double& torpedoVelocityY, double& torpedoAccelerationY)
    {
        torpedoVelocityY += (torpedoAccelerationY * deltat);
    }

    //Calculates Torpedo's x position
    //TorpedoPositionX(n) = TorpedoPositionX(n-1) + (TorpedoVelocityX * deltat)
    void calculateTorpedoPositionX(double& positionX, double& velocityX)
    {
        positionX += (velocityX * deltat);
    }

    //Calculates Torpedo's y position
    //TorpedoPositionY(n) = TorpedoPositionY(n-1) + (TorpedoVelocityY * deltat)
    void calculateTorpedoPositionY(double& positionY, double& velocityY)
    {
        positionY += (velocityY * deltat);
    }

    //function to make sure user inputs a double for electric charge, rejects other variable types
    double getChargeNumber(int shieldNum)
    {
        /* For automatically setting charges
        switch (shieldNum)
        {
        case 1:
            return .0001;
        case 2:
            return .0001;
        case 3:
            return .0001;
        case 4:
            return .0001;
        case 5:
            return .0004;
        case 6:
            return .0004;
        case 7:
            return .0001;
        case 8:
            return .0001;
        case 9:
            return .0001;
        case 10:
            return .0001;
        }
        */
        bool enteredFloat = false;
        double enteredCharge = NULL;
        while (!enteredFloat)
        {
            cin >> enteredCharge;
            if (cin.fail())
            {
                cin.clear();
                cin.ignore(1000, '\n');
                cout << "Invalid number, enter a float (decimal) number.\n";
                cout << "Re-enter Deflecter Shield " << shieldNum << " Charge: ";
                enteredCharge = NULL;
            }
            else
            {
                return enteredCharge;
            }
        }
    }
    
    //checks that the sum of the shield charges = +1.6 mC
    void checkChargesAreValid()
    {
        if (current != head)
        {
            current = head;
        }

        int totalCharge = 0;

        for (int shield = 0; shield < 10; shield++)
        {
            totalCharge += (10000 * current->electricCharge);
            current = current->next;
        }

        if (totalCharge != 16)
        {
            cout << "ERROR: The total charges of the shields do not = +1.6 mC, enter new valid charges.\n\n";
            deleteShieldsInList();
            getDeflectorShieldCharges();
        }
        totalCharge = NULL;
    }

    //get all charges of deflector shields from user and calculates all values for t=0s
    void getDeflectorShieldCharges()
    {
        double currentCharge = NULL, currentForce = NULL, currentForceX = NULL, currentForceY = NULL,
            currentHeight = 50.0, currentDistance = NULL, currentElectricField = NULL; //values to be obtained and entered into shield

            for (int currentShield = 1; currentShield <= 10; currentShield++) //loop that repeats 10 times to fill list of shields
            {
                if (head != NULL) //head exists (currentShield > 1)
                {
                    current = current->next; //increments pointer to next shield
                    cout << "Deflector Shield " << currentShield << " Charge: ";
                    currentCharge = getChargeNumber(currentShield);
                    calculateDistance(currentDistance, currentHeight, torpedoPositionX, torpedoPositionY);
                    calculateElectricField(currentElectricField, currentCharge, currentDistance);
                    calculateShieldForceOnTorpedo(currentForce, currentForceX, currentForceY, torpedoPositionX, torpedoPositionY, currentHeight, currentElectricField);

                    current->shieldNumber = currentShield;
                    current->electricCharge = currentCharge;
                    current->forceOnTorpedo = currentForce;
                    current->forceOnTorpedoX = currentForceX;
                    current->forceOnTorpedoY = currentForceY;
                    current->height = currentHeight;
                    current->distanceFromTorpedo = currentDistance;
                    current->electricField = currentElectricField;
                    
                    if (currentShield != 10)
                    {
                        current->next = new DeflectorShield;
                    }
                    else
                    {
                        current->next = head; //links list back to first deflector shield after list is filled
                        current = current->next; //moves pointer back to head
                        checkChargesAreValid();
                    }
                }
                else //head does not exist yet (currentShield = 1)
                {
                    head = new DeflectorShield;
                    cout << "Enter the electric charges in terms of C for the Deflector shields in order from bottom to top, separated by \"Enter\":\n";
                    cout << "Deflector Shield " << currentShield << " Charge: ";
                    currentCharge = getChargeNumber(currentShield);

                    calculateDistance(currentDistance, currentHeight, torpedoPositionX, torpedoPositionY);
                    calculateElectricField(currentElectricField, currentCharge, currentDistance);
                    calculateShieldForceOnTorpedo(currentForce, currentForceX, currentForceY, torpedoPositionX, torpedoPositionY, currentHeight, currentElectricField);

                    head->shieldNumber = currentShield;
                    head->electricCharge = currentCharge;
                    head->forceOnTorpedo = currentForce;
                    head->forceOnTorpedoX = currentForceX;
                    head->forceOnTorpedoY = currentForceY;
                    head->height = currentHeight;
                    head->distanceFromTorpedo = currentDistance;
                    head->electricField = currentElectricField;
                    head->next = new DeflectorShield;
                    current = head;
                }
                currentHeight += 100.0;
            }
    }

        //prints all the values of each deflector shield
        void printAllDeflectorShields()
        {
           if (current != head)
            {
                current = head;
            }

           cout << "At time = 0s:\n";
           cout << "Shield #   Electric Charge(C) Force on Torpedo(N)    Height(m)  Distance From Torpedo(m)   Electric Field(N/C) Force on TorpedoX (N) Force on TorpedoY (N)\n";

            do
            {
                cout << setprecision(1) << current->shieldNumber << "\t\t";
                cout << scientific << current->electricCharge << "\t\t";
                cout << setprecision(6) << current->forceOnTorpedo << "\t\t";
                cout << setprecision(4) << defaultfloat << current->height << "\t\t";
                cout << setprecision(2) << fixed << current->distanceFromTorpedo << "            ";
                cout << setprecision(4) << scientific << current->electricField << "            ";
                cout << current->forceOnTorpedoX << "            ";
                cout << current->forceOnTorpedoY << "\n\n";
                
                current = current->next;
            }while (current != head);

        }

        //Runs simulation of torpedo hitting wall, outputting all values
        //within each 0.01s time frame, passes variables by value so the 
        //global (t=0s) variables remain unchanged
        void runTorpedoSimulation(double torpedoAccelerationX, double torpedoAccelerationY,double torpedoPositionX, double torpedoPositionY, 
            double torpedoVelocityX, double torpedoVelocityY, double netForceOnTorpedoX, double netForceOnTorpedoY)
        {
            double timeElapsed = 0.00;

            cout << "\nNow running torpedo simulation...\n\n";
            cout << "Constants are:\ndeltat = 0.01 s\nTorpedo Mass = .5 kg\nTorpedo Charge = +16 C\n"
                << "k = Electrostatic Constant = 1/(4(pi)(permitivity const)) = 8.99x10E9 N*m^2/C^2\n"
                << "Heights from bottom up = {50, 150, 250, 350, 450, 550, 650, 750, 850, 950} m\n";
            cout << "Charges from bottom up = {+";
            if (current != head)
            {
                current = head;
            }

            for (int shield = 0; shield < 10; shield++)
            {
                cout << setprecision(0) << current->electricCharge;

                if (shield != 9)
                {
                    cout << ", +";
                }

                current = current->next;
            }

            cout << "} C\n";
            cout << "Equations at datapoint n are:\nTime(n) = Time(n-1) + deltat s\n"
                << "Es = Electric Field(shield) = k|q(Shield)|/r^2 N/C\n"
                << "Fs = Shield Force on Torpedo = |q(Torpedo)|E N\n"
                << "PosX = TorpedoPositionX(n) = TorpedoPositionX(n-1) + (TorpedoVelocityX(n) * deltat) m\n"
                << "PosY = TorpedoPositionY(n) = TorpedoPositionY(n-1) + (TorpedoVelocityY(n) * deltat) m\n"
                << "VelX = TorpedoVelocityX(n) = TorpedoVelocityX(n-1) + (TorpedoAccelerationX(n) * deltat) m/s\n"
                << "VelY = TorpedoVelocityY(n) = TorpedoVelocityY(n-1) + (TorpedoAccelerationY(n) * deltat) m/s\n"
                << "AccX = TorpedoAccelerationX = NetForceOnTorpedoX / TorpedoMass m/s^2\n"
                << "AccY = TorpedoAccelerationY = NetForceOnTorpedoY / TorpedoMass m/s^2\n"
                << "FnetX = NetForceOnTorpedoX = (i=1)SUM(to 10) (ForceOnTorpedo * cos(theta)) N\n"
                << "FnetY = NetForceOnTorpedoY = (i=1)SUM(to 10) -(ForceOnTorpedo * sin(theta)) N [(-) for opposite y-direction]\n"
                << "theta = arctan((ShieldPositionY - TorpedoPositionY) / TorpedoPositionX)\n";
            cout << "\nTime Es(1)   Fs(1)   Es(2)   Fs(2)   Es(3)   Fs(3)   Es(4)   Fs(4)   Es(5)   Fs(5)   Es(6)   Fs(6)   Es(7)   "
                << "Fs(7)   Es(8)   Fs(8)   Es(9)   Fs(9)   Es(10)  Fs(10)  PosX     PosY    VelX    VelY AccX AccY FnetX FnetY\n";
            
            //display all values at t=0s
            cout << setprecision(2) << fixed << timeElapsed << " ";
            cout << setprecision(1) << scientific;
            do
            {
                SetConsoleTextAttribute(hConsole, 11);
                cout << current->electricField << " ";
                SetConsoleTextAttribute(hConsole, 10);
                cout << setprecision(1) << scientific << current->forceOnTorpedo << " ";
                current = current->next;
            } while (current != head);

            calculateNetForceOnTorpedoX(netForceOnTorpedoX);
            calculateNetForceOnTorpedoY(netForceOnTorpedoY);
            calculateTorpedoAccelerationX(torpedoAccelerationX, netForceOnTorpedoX);
            calculateTorpedoAccelerationY(torpedoAccelerationY, netForceOnTorpedoY);

            SetConsoleTextAttribute(hConsole, 2);
            cout << setprecision(1) << fixed << torpedoPositionX << "  ";
            SetConsoleTextAttribute(hConsole, 8);
            cout << setprecision(2) << fixed << torpedoPositionY << " ";
            SetConsoleTextAttribute(hConsole, 3);
            cout << torpedoVelocityX << " ";
            SetConsoleTextAttribute(hConsole, 4);
            cout << torpedoVelocityY << " ";
            SetConsoleTextAttribute(hConsole, 5);
            cout << torpedoAccelerationX << " ";
            SetConsoleTextAttribute(hConsole, 6);
            cout << torpedoAccelerationY << " ";
            SetConsoleTextAttribute(hConsole, 7);
            cout << netForceOnTorpedoX << " ";
            SetConsoleTextAttribute(hConsole, 12);
            cout << netForceOnTorpedoY << endl;
            SetConsoleTextAttribute(hConsole, 7);

            timeElapsed += deltat;

            //loop to display all values in 0.01s intervals
            while (timeElapsed < 10.00)
            {
                cout << setprecision(2) << fixed << timeElapsed << " ";
                cout << setprecision(1) << scientific;
                //loop to calculate all deflector shield electric charges and forces on torpedo
                do
                {
                    calculateDistance(current->distanceFromTorpedo, current->height, torpedoPositionX, torpedoPositionY);
                    calculateElectricField(current->electricField, current->electricCharge, current->distanceFromTorpedo);
                    calculateShieldForceOnTorpedo(current->forceOnTorpedo, current->forceOnTorpedoX, current->forceOnTorpedoY, 
                        torpedoPositionX, torpedoPositionY, current->height, current->electricField);
                    SetConsoleTextAttribute(hConsole, 11);
                    cout << current->electricField << " ";
                    SetConsoleTextAttribute(hConsole, 10); 
                    cout << current->forceOnTorpedo << " ";
                    current = current->next;
                } while (current != head);

                calculateNetForceOnTorpedoX(netForceOnTorpedoX);
                calculateNetForceOnTorpedoY(netForceOnTorpedoY);
                calculateTorpedoAccelerationX(torpedoAccelerationX, netForceOnTorpedoX);
                calculateTorpedoAccelerationY(torpedoAccelerationY, netForceOnTorpedoY);
                calculateTorpedoVelocityX(torpedoVelocityX, torpedoAccelerationX);
                calculateTorpedoVelocityY(torpedoVelocityY, torpedoAccelerationY);
                calculateTorpedoPositionX(torpedoPositionX, torpedoVelocityX);
                calculateTorpedoPositionY(torpedoPositionY, torpedoVelocityY);

                SetConsoleTextAttribute(hConsole, 2);
                cout << setprecision(2) << fixed << torpedoPositionX << "  ";
                SetConsoleTextAttribute(hConsole, 8);
                cout << torpedoPositionY << " ";
                SetConsoleTextAttribute(hConsole, 3); 
                cout << torpedoVelocityX << " ";
                SetConsoleTextAttribute(hConsole, 4); 
                cout << torpedoVelocityY << " ";
                SetConsoleTextAttribute(hConsole, 5); 
                cout << torpedoAccelerationX << " ";
                SetConsoleTextAttribute(hConsole, 6); 
                cout << torpedoAccelerationY << " ";
                SetConsoleTextAttribute(hConsole, 7);
                cout << netForceOnTorpedoX << " ";
                SetConsoleTextAttribute(hConsole, 12); 
                cout << netForceOnTorpedoY << endl;
                SetConsoleTextAttribute(hConsole, 7);

                timeElapsed += deltat;
            }
            cout.precision(6);
        }

        //reads instructions from the user and executes them
        void processInstructions()
        {
            string instruction;
            bool running = true;
            cout << "Enter an Instruction (Commands are: printshields, newshields, run, help, exit): ";
            cin >> instruction;
            for_each(instruction.begin(), instruction.end(), [](char& c)
            {
                c = ::tolower(static_cast<unsigned char>(c));
            });

            while (running)
            {
                if (instruction == "printshields")
                {
                    printAllDeflectorShields();
                    instruction = "";
                }
                else if (instruction == "newshields")
                {
                    deleteShieldsInList();
                    getDeflectorShieldCharges();
                    instruction = "";
                }
                else if (instruction == "run")
                {
                    runTorpedoSimulation(torpedoAccelerationX, torpedoAccelerationY, torpedoPositionX, torpedoPositionY, 
                        torpedoVelocityX, torpedoVelocityY, netForceOnTorpedoX, netForceOnTorpedoY);
                    resetShieldVariables();
                    instruction = "";
                }
                else if (instruction == "exit")
                {
                    running = 0;
                    instruction = "";
                    return;
                }
                else if (instruction == "help")
                {
                    cout << "printshields to print variables of every shield\n";
                    cout << "newshields to delete current shields and replace with new charges\n";
                    cout << "run to run the torpedo simulation\n";
                    cout << "exit to exit the program\n";
                    instruction = "";
                }
                else
                {
                    cout << "Invalid instruction, enter help to see valid instructions";
                    cin.clear();
                    cin.ignore(1000, '\n');
                    instruction = "";
                }

                cout << "\nEnter an instruction: ";
                cin >> instruction;
            }
        }
};

int main()
{
    hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hConsole, 7);
    cout << "PHYS 202 Lab 2: Coulomb Torpedo Attack\nTimothy Evans & Jake Dannewitz\n\n";

    DeflectorShieldList shieldList;
    shieldList.getDeflectorShieldCharges();
    shieldList.processInstructions();
    shieldList.deleteShieldsInList();

    return 0;
}
